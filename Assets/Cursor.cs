﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursor : MonoBehaviour
{
    public bool Overlay;
    public UnityEngine.UI.Image image;
    public Color PressedColor, UnpressedColor;
    private RectTransform r;
    // Start is called before the first frame update
    void Start() {
        UnityEngine.Cursor.visible = false;
        if (Overlay) {
            r = GetComponent<RectTransform>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!Overlay) { 
            float z = transform.position.z;
            Vector3 v = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            v.z = z;
            transform.position = v;
        }
        else {
            Vector3 v = Input.mousePosition;
            v.z = r.position.z;
            r.position = v;
            if (Input.GetButton("Fire1")) {
                image.color = PressedColor;
            }
            else {
                image.color = UnpressedColor;
            }
        }
    }
}
