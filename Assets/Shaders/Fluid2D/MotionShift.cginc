
//shift scale
float _ShiftScale;
float _StretchScale;
int active;
float4x4 _Last_M;
float4x4 _M;
float4x4 _Last_VP;
float invDT;

struct MotionVertexInput
{
    float4 vertex : POSITION;
    float3 oldPos : NORMAL;
};
 
struct V2F
{
    float4 pos : SV_POSITION;
    float3 vec : TEXCOORD0;
};

//Vertex Shift vert function
V2F vertex(MotionVertexInput i)
{
    V2F o;
    
    float3 currWPos = mul(unity_ObjectToWorld, i.vertex);
    float3 centerPos = mul(unity_ObjectToWorld, float4(0, 0, 0,1));
    float3 prevWPos = mul(_Last_M, i.vertex);
    float mag = length((currWPos - centerPos).xy) + 1e-4;
    float3 vertNorm = float3((currWPos - centerPos).xy / mag,0);
    o.vec = (currWPos - prevWPos);
    float vecLength = length(o.vec) + 1e-4;
    float3 vecNorm = o.vec / vecLength;
    float3 sqrtVec = vecNorm * (vecLength);
    float scale = dot(vecNorm.xy, vertNorm.xy);
    
    o.pos = mul(UNITY_MATRIX_VP, float4(currWPos + sqrtVec * _ShiftScale * active + vecNorm * scale * _StretchScale * active, 1));
    o.vec *= invDT * active;
    return o;
}

//Fragment function
fixed4 fragment(V2F i) : SV_Target
{
    return fixed4(i.vec, 1);
}
