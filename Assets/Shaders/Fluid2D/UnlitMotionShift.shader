﻿Shader "Unlit/UnlitMotionShift"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_ShiftScale("Shift Scale", Float) = 1
		_StretchScale("Stretch Scale", Float) = 10
    }
    SubShader
    {
        Pass
        {
			Tags{ "Queue" = "Opaque" "RenderType" = "Opaque" "LightMode" = "ForwardBase" }
			LOD 100
			ZWrite On
			Cull Off
			ZTest LEqual
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
			#pragma target 3.0 

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };
			
            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                return col;
            }
            ENDCG
        }

		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
			
		Pass{
			Tags{ "LightMode" = "MotionVectors" }
			Cull Off
			ZWrite On
			ZTest LEqual
			CGPROGRAM
			#pragma vertex vertex
			#pragma fragment fragment
			
			#include "UnityCG.cginc"
			#include "MotionShift.cginc"
			ENDCG
		}
    }
}
