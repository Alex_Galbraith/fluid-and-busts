﻿Shader "Hidden/FluidBlit"
{
	Properties
	{
		[HideInInspector]
		_MainTex("Texture", 2D) = "white" {}

		_Smoke("Smoke Strength",Float) = 0
		_SmokeDepth("Smoke Depth", Float) = 0

	}
		SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always
		Blend One Zero
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

			sampler2D _MainTex;
			sampler3D _ColorTex;
			sampler2D _CameraDepthTexture;
			sampler2D _SolidTexture;
			float _Smoke, _SmokeDepth;
			float uvz;
			fixed4 frag(v2f i) : SV_Target
			{
				fixed4 fcol = tex2D(_MainTex, i.uv);
				fixed4 solid = tex2D(_SolidTexture, i.uv);
				fixed4 scol = tex3D(_ColorTex, float3(i.uv, uvz));
				scol.rgb = normalize(scol.rgb + 0.001);

				half depth = tex2D(_CameraDepthTexture, i.uv).r;
				//linear depth between camera and far clipping plane
				depth = Linear01Depth(depth);
				float showSmoke = step(_SmokeDepth, depth);
				scol.rgb *= 1 + max(0, scol.a * _Smoke - 1);
				scol.a *= _Smoke;
				fixed4 outc = saturate(scol) * showSmoke;

				//return saturate(float4(fcol.rgb * (1 - outc.a) + outc * outc.a,1));
				return saturate(lerp(float4(fcol * (1-outc.a) + outc.rgb * outc.a,1),float4(1,1,1,1),step(0.1,solid.r)));
			}
			ENDCG
		}
	}
}
