﻿Shader "Custom/Dissolve"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Dissolve("Dissolve", 2D) = "white" {}
		_Ramp("Ramp", 2D) = "transparent" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_DissolveVal("Dissolve Value", Range(0,2)) = 0
		_DissolveScale("Dissolve Scale", Float) = 1
		_Brightness("Brightness Scale", Float) = 1
    }
    SubShader
    {
        Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
        LOD 200
		Cull Off
        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows addshadow 

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0
        sampler2D _MainTex, _Dissolve, _Ramp;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
		
		float _DissolveVal, _DissolveScale, _Brightness;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
			float2 newUV = IN.uv_MainTex;
            fixed4 c = tex2D (_MainTex, newUV) * _Color;
			fixed d = tex2D(_Dissolve, newUV*_DissolveScale).r + 1;
			clip(d - _DissolveVal);
			float rd = saturate(d - _DissolveVal);
			fixed4 dcolor = tex2D(_Ramp, float2(1- rd, 0));
			c = lerp(c, float4(dcolor.rgb,1), saturate(dcolor.a));
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
			o.Emission = saturate(dcolor) * saturate(dcolor.a) * _Brightness;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
