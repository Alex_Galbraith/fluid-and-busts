﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/Ripple"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
		_Strength("Strength",Float) = 0
		_Tightness("Tightness",Float) = 0
		_Brightness("Brightness",Float) = 1
		_Dampen("Dampen",Float) = 1
		_Max("Max",Float) = 1
		_Speed("Speed",Float) = 1
		_DistanceScale("DistanceScale",Float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert addshadow

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct surfIn
        {
            float2 uv_MainTex;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;
		fixed3 _Point;
		float _Radius, _Strength, _Tightness, _Brightness, _Dampen, _Max, _Speed, _DistanceScale;

		#define MAX_POINTS 10
		float3 _points[10];
		float _times[10];
		float _time;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

		struct Input {
			float2 uv_MainTex;
			INTERNAL_DATA
			float3 worldPos;
		};

		void vert(inout appdata_full v) {
			fixed4 wpos = mul(unity_ObjectToWorld, v.vertex);
			for (int i = 0; i < MAX_POINTS; i++) {
				float dist = (distance(wpos, _points[i])*_DistanceScale);
				float sdist = clamp(dist / (_time - _times[i])*_Speed, 0, 1);
				float p = min(_Max, pow((0.5 - cos(sdist * 3.14159f * 2) / 2), _Tightness)*dist*_Strength / pow(dist, _Dampen));
				v.vertex.xyz += v.normal * p;
			}
			
		}

		

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			float p = 0;
			//Calculate emission
			for (int i = 0; i < MAX_POINTS; i++) {
				float dist = (distance(IN.worldPos, _points[i])*_DistanceScale);
				float sdist = clamp(dist / (_time - _times[i])*_Speed, 0, 1);
				p += min(_Max, pow((0.5 - cos(sdist * 3.14159f * 2) / 2), _Tightness)*dist*_Strength / pow(dist, _Dampen));
			}
			

            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
			o.Emission = p * _Color * _Brightness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
