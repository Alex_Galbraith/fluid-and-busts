﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemRewinder : MonoBehaviour
{
    public ParticleSystem ps;
    public float time;
    // Update is called once per frame
    void Update()
    {
        ps.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        ps.Play(false);
        ps.Simulate(time, false, false, true);
    }
}
