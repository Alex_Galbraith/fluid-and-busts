﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
public class FluidForceSampler2D : MonoBehaviour
{
    public float speedStrength = 0.8f;
    public float pressureStrength = 0.01f;
    [Serializable]
    public struct ForcePoint {
        public Transform point;
        public float forceScale;
    }

    public FluidPost fluid;
    Rigidbody2D RB;
    public ForcePoint[] forcePoints;

    // Start is called before the first frame update
    void Start() {
        RB = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var fp in forcePoints) {
            if (fluid.SamplePoint(fp.point.position, out Vector4 vec, out Vector2 fpos)){
                Vector2 vel = (Vector2)vec;
                vel = fluid.FluidVelToWorldVel(vel, fpos, transform.position.z);
                Vector2 delta = (vel - RB.GetPointVelocity(fp.point.position)) ;
                delta = delta * (Mathf.Max(0,Vector2.Dot(-fp.point.up, vel.normalized))*0.5f+0.5f);
                RB.AddForceAtPosition(delta* fp.forceScale * speedStrength, fp.point.position);
                RB.AddForceAtPosition(-fp.point.up * fp.forceScale * vec.w * pressureStrength, fp.point.position);
            }
        }
    }
}
