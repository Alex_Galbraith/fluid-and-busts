﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionVectorHelper : MonoBehaviour
{
    private MaterialPropertyBlock block;
    private Material mMaterial;
    private Renderer mRenderer;
    private Matrix4x4 lastModel;

    public bool Active = true;
    public void SetActive(bool a) {
        Active = a;
        mRenderer.GetPropertyBlock(block);
        block.SetInt("active", Active ? 1 : 0);
        mRenderer.SetPropertyBlock(block);
    }

    private void Awake() {
        block = new MaterialPropertyBlock();
        mRenderer = GetComponent<Renderer>();
        mMaterial = mRenderer.material;
        mRenderer.motionVectorGenerationMode = MotionVectorGenerationMode.Object;
        
    }

    private void Start() {
        mRenderer.GetPropertyBlock(block);
        block.SetMatrix("_Last_M", mRenderer.localToWorldMatrix);
        block.SetMatrix("_M", mRenderer.localToWorldMatrix);
        block.SetFloat("invDT", 1 / Time.fixedDeltaTime);
        block.SetInt("active", 0);
        mRenderer.SetPropertyBlock(block);
        Invoke("LateAwake", 0.1f);
    }
    private void LateAwake() {
        mRenderer.GetPropertyBlock(block);
        block.SetInt("active", Active?1:0);
        block.SetFloat("invDT", 1 / Time.fixedDeltaTime);
        block.SetMatrix("_Last_M", mRenderer.localToWorldMatrix);
        mRenderer.SetPropertyBlock(block);
    }
    private void FixedUpdate() {
        mRenderer.GetPropertyBlock(block);
        block.SetFloat("invDT", 1 / Time.fixedDeltaTime);
        block.SetMatrix("_Last_M", lastModel == null ? mRenderer.localToWorldMatrix : lastModel);
        block.SetMatrix("_M", mRenderer.localToWorldMatrix);
        mRenderer.SetPropertyBlock(block);
        lastModel = mRenderer.localToWorldMatrix;
    }

}
