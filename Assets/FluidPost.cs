﻿using UnityEngine;
using Unity.Mathematics;
using UnityEngine.Rendering;
using Random = UnityEngine.Random;
using Unity.Collections;
using System;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class FluidPost : MonoBehaviour
{
    public Material renderWith;
    public GameObject ToSpawn;
    public SpriteRenderer Cursor;
    Camera cam;
    public Vector2Int textureSize = new Vector2Int(960, 540);
    public ComputeShader compute;
    [Header("Emission")]
    public Vector2 Speed = Vector2.right;
    public float Pressure = 5, Smoke = 5, Randomness = 0, depthCutoff = 0.1f, vorticity = 0.1f, visualPressure = 0.05f;

    //Mouse Emission
    [Header("Mouse Emission")]
    public Color mouseEmitCenter;
    public Color mouseEmitOuter;
    public float mouseEmitSpeed;
    public int mouseEmitRadius;

    #region accessors
    public Color MouseEmitCenter { get => mouseEmitCenter; set => mouseEmitCenter = value; }
    public Color MouseEmitOuter { get => mouseEmitOuter; set => mouseEmitOuter = value; }
    public float MouseEmitSpeed { get => mouseEmitSpeed; set => mouseEmitSpeed = value; }
    public float MouseEmitRadius { get => mouseEmitRadius; set => mouseEmitRadius = (int)value; }


    public float Vorticity {
        get { return vorticity; }
        set { vorticity = value; }
    }

    public float SmokeDens {
        get { return Smoke; }
        set { Smoke = value; }
    }

    public enum ControlType {
        SOLID,
        SPAWN,
        INTERACT,
        EMIT
    }

    public ControlType Control {
        get;
        set;
    }

    public float XSpeed {
        set {
            Speed.x = value;
        }
    }

    public float YSpeed {
        set {
            Speed.y = value;
        }
    }

    public float SetPressure {
        set {
            Pressure = value;
        }
    }

    public float EmissionRandomness {
        set {
            Randomness = value;
        }
    }

    public float FogStrength {
        set {
            renderWith.SetFloat("_Smoke", value);
        }
    }

    public float VisualPressure {
        set {
            visualPressure = value;
            compute.SetFloat("visualPressure", visualPressure);
        }
    }

    public int ControlI {
        set {
            Control = (ControlType)value;
        }
    }

    

    public void ResetScene() {
        SceneManager.LoadScene("Fluid");
    }

    #endregion

    RenderTexture SolidTexture, ColorTexture;
    ComputeBuffer FogData, ReadbackBuffer;
    float4[] data;
    float4[] altData;
    private bool requesting;

    private Matrix4x4 CameraToFluid, FluidToCamera;

    int Kernel, SingleKernel, ReadbackKernel;

    void Awake() {
        cam = GetComponent<Camera>();
        cam.depthTextureMode = DepthTextureMode.MotionVectors | DepthTextureMode.Depth;

        lastTime = Time.time - 0.01f;
        CreateTextures();

        ReadbackKernel = compute.FindKernel("ReadBackCopy");
        compute.SetBuffer(ReadbackKernel, "FogDat", FogData);
        compute.SetBuffer(ReadbackKernel, "ReadBack", ReadbackBuffer);

        int InitKern = compute.FindKernel("Init");
        compute.SetBuffer(InitKern, "FogDat", FogData);
        compute.SetTexture(InitKern, "Color", ColorTexture);
        compute.SetInt("sizeX", textureSize.x);
        compute.SetInt("sizeY", textureSize.y);
        compute.Dispatch(InitKern, textureSize.x/16, textureSize.y / 16, 2);
        
        compute.SetFloat("vorticity", vorticity);
        compute.SetFloat("visualPressure", visualPressure);

        Kernel = compute.FindKernel("CSMain");
        SingleKernel = compute.FindKernel("Single");

        data = new float4[textureSize.x * textureSize.y];
        altData = new float4[textureSize.x * textureSize.y];

        CameraToFluid = ComputeCamerToFluidMatrix();
        FluidToCamera = ComputeFluidToCameraMatrix();
        Control = ControlType.EMIT;
    }

    private void CreateTextures() {
        FogData = new ComputeBuffer(textureSize.x * textureSize.y * 2, 4 * 4, ComputeBufferType.Default);
        ReadbackBuffer = new ComputeBuffer(textureSize.x * textureSize.y, 4 * 4, ComputeBufferType.Default);

        SolidTexture = new RenderTexture(textureSize.x, textureSize.y, 0) {
            useMipMap = false,
            enableRandomWrite = true,
            volumeDepth = 0,
            format = RenderTextureFormat.ARGBHalf,
            filterMode = FilterMode.Trilinear
        };
        SolidTexture.Create();

        ColorTexture = new RenderTexture(textureSize.x, textureSize.y, 0) {
            useMipMap = false,
            enableRandomWrite = true,
            volumeDepth = 2,
            format = RenderTextureFormat.ARGBHalf,
            filterMode = FilterMode.Trilinear,
            dimension = UnityEngine.Rendering.TextureDimension.Tex3D
        };
        ColorTexture.Create();

    }

    float lastTime;
    private void GPUCallback(AsyncGPUReadbackRequest req) {
        if (!req.hasError) {
            req.GetData<float4>().CopyTo(altData);
            lock (this) {
                var temp = altData;
                altData = data;
                data = temp;
            }
        }
        else {
            Debug.LogError("Request error");
        }
        requesting = false;
        ExtractData();
    }

    private void ExtractData() {
        if (requesting)
            return;
        requesting = true;

        int xBatches, yBatches;
        xBatches = Mathf.CeilToInt(textureSize.x / 32f);
        yBatches = Mathf.CeilToInt(textureSize.y / 32f);

        compute.Dispatch(ReadbackKernel, xBatches, yBatches, 1);

        try {
            AsyncGPUReadback.Request(ReadbackBuffer, GPUCallback);
        }
        catch (Exception e) { Debug.LogError(e); }
    }

    private Matrix4x4 ComputeCamerToFluidMatrix() {
        return Matrix4x4.Scale(new Vector3((float)textureSize.x / (float)cam.pixelWidth, (float)textureSize.y / (float)cam.pixelHeight, 1));
    }

    private Matrix4x4 ComputeFluidToCameraMatrix() {
        return Matrix4x4.Scale(new Vector3((float)cam.pixelWidth / (float)textureSize.x, (float)cam.pixelHeight / (float)textureSize.y, 1));
    }

    public Vector2 FluidVelToWorldVel(Vector2 velocity, Vector2 fpos, float depth) {
        Vector4 v = FluidToCamera.MultiplyVector(velocity);
        Vector4 p = FluidToCamera * fpos;
        v.z = p.z = depth;

        Vector4 delta = cam.ScreenToWorldPoint(p + v) - cam.ScreenToWorldPoint(p);

        return delta;
    }
    
    public bool SamplePoint(Vector3 wpos, out Vector4 o, out Vector2 fpos, int sampleRadius = 1) {
        o = Vector4.zero;
        fpos = Vector2.zero;
        if (data == null)
            return false;

        sampleRadius = Mathf.Abs(sampleRadius);

        Vector3 cpos = cam.WorldToScreenPoint(wpos);
        fpos = CameraToFluid.MultiplyPoint(cpos);
        return SamplePointFluidSpace(fpos, out o, sampleRadius);
    }

    public bool SamplePointFluidSpace(Vector3 fpos, out Vector4 o, int sampleRadius = 1) {
        o = Vector4.zero;

        if (fpos.x < 0+ sampleRadius || fpos.y < 0+ sampleRadius || fpos.x >= textureSize.x- sampleRadius || fpos.y >= textureSize.y- sampleRadius)
            return false;
        if (data == null)
            return false;

        sampleRadius = Mathf.Abs(sampleRadius);

        int pos1 = (int)(fpos.x) + ((int)fpos.y * textureSize.x);
        float4 sum = new float4();
        for (int i = -sampleRadius; i <= sampleRadius; i++)
            for (int j = -sampleRadius; j <= sampleRadius; j++) {
                int np1 = pos1 + i + j * textureSize.x;
                lock (this) {
                    float4 d1 = data[np1];
                    sum += d1 ;
                }
            }

        o = sum / ((sampleRadius * 2 + 1) * (sampleRadius * 2 + 1));
        return true;
    }

    private void Update() {
        Vector2 p = CameraToFluid.MultiplyPoint(new Vector4(Input.mousePosition.x, Input.mousePosition.y, 0, 1));
        Vector3 wpos = cam.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mvel = CameraToFluid.MultiplyVector(new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")));
        wpos.z = 10;
        if(EventSystem.current.currentSelectedGameObject == null) {
            switch (Control) {
                case ControlType.SOLID:
                    if (Input.GetButton("Fire1")) {
                        compute.SetVector("drawSolid", p);
                        compute.SetInt("doDraw", 1);
                    }
                    else {
                        compute.SetInt("doDraw", 0);
                    }
                    break;
                case ControlType.SPAWN:
                    if (Input.GetButtonUp("Fire1")) {
                        var g = Instantiate(ToSpawn, wpos, Quaternion.Euler(0, 0, Random.Range(0, 360.0f)));
                        g.GetComponent<FluidForceSampler2D>().fluid = this;
                        
                    }
                    break;
                case ControlType.INTERACT:
                    if (Input.GetButton("Fire1")) {
                        Cursor.enabled = true;
                    }
                    else {
                        Cursor.enabled = false;
                    }
                    break;
                case ControlType.EMIT:
                    if (Input.GetButton("Fire1")) {
                        compute.SetBool("doEmit", true);
                        compute.SetVector("emitFrom", p);
                        compute.SetVector("emitVelocity", -mvel* MouseEmitSpeed);
                        compute.SetInt("radius", mouseEmitRadius);
                        compute.SetVector("colorCenter", MouseEmitCenter);
                        compute.SetVector("colorOuter", MouseEmitOuter);
                    }
                    else {
                        compute.SetBool("doEmit", false);
                    }
                    break;
            }
        }


        Vector2 r = Random.insideUnitCircle * Randomness;
        compute.SetVector("emit", new Vector4(100, 100, Speed.x + r.x , Speed.y + r.y));
        compute.SetVector("emitDat", new Vector4(Smoke, Pressure, 0, 0));
    }

    private bool flipped;
    private void FlipTextures() {
        compute.SetInt("FID", flipped ? 1 : 0);
        compute.SetInt("BID", flipped ? 0 : 1);
        flipped = !flipped;
    }

    private void SetTextures() {
        compute.SetTexture(SingleKernel, "Solid", SolidTexture);
        compute.SetTexture(SingleKernel, "Color", ColorTexture);
        compute.SetBuffer(SingleKernel, "FogDat", FogData);


        compute.SetTexture(Kernel, "Solid", SolidTexture);
        compute.SetTexture(Kernel, "Color", ColorTexture);
        compute.SetBuffer(Kernel, "FogDat", FogData);
        compute.SetTextureFromGlobal(Kernel, "Depth", "_CameraDepthTexture");
        compute.SetTextureFromGlobal(Kernel, "Motion", "_CameraMotionVectorsTexture");

        compute.SetInt("FID", flipped ? 1 : 0);
        compute.SetInt("BID", flipped ? 0 : 1);
    }

    private void RunCompute() {
        int xBatches, yBatches;
        xBatches = Mathf.CeilToInt(textureSize.x / 32f);
        yBatches = Mathf.CeilToInt(textureSize.y / 32f);
        Matrix4x4 mat = FluidToCamera;
        compute.SetMatrix("FluidToCamera", mat);
        compute.SetFloat("vorticity", vorticity);
        compute.SetTextureFromGlobal(Kernel, "Depth", "_CameraDepthTexture");
        compute.SetTextureFromGlobal(Kernel, "Motion", "_CameraMotionVectorsTexture");
        compute.SetFloat("depthCutoff", depthCutoff);
        compute.Dispatch(SingleKernel, 1, 1, 1);
        compute.Dispatch(Kernel, xBatches, yBatches, 1);
        
    }

    // Postprocess the image
    void OnRenderImage(RenderTexture source, RenderTexture destination) {
        
        renderWith.SetTexture("_ColorTex", ColorTexture);
        renderWith.SetTexture("_SolidTexture", SolidTexture);
        renderWith.SetFloat("uvz", flipped ? 0f : 1f);
        Graphics.Blit(source, destination, renderWith);
        
    }

    private bool first;
    private void OnPostRender() {
        SetTextures();
        compute.SetFloat("TIMESTEP", Mathf.Min(0.01f, Time.deltaTime));
        RunCompute();
        if (!requesting)
            ExtractData();
        FlipTextures();
    }

    private void OnDestroy() {
        FogData.Dispose();
        ReadbackBuffer.Dispose();
    }
}
