﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rippler : MonoBehaviour
{
    private static int MAX_POINTS = 10;
    MeshCollider mCollider;
    public Material material;
    int lastRipple = 0;
    Vector4[] points = new Vector4[MAX_POINTS];
    float[] times = new float[MAX_POINTS];
    float lastTime;
    public float averageDelay = 0.3f;
    public ParticleSystem particleSystem;

    // Start is called before the first frame update
    void Start()
    {
        mCollider = GetComponent<MeshCollider>();
        material = GetComponent<Renderer>().sharedMaterial;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - lastTime > averageDelay + (Random.value*averageDelay - averageDelay/2)) {
            Vector3 point = Random.insideUnitSphere*5 + transform.position;
            
            Ray r = new Ray(point, (transform.position - point));
            mCollider.Raycast(r, out RaycastHit hit, 5);
            particleSystem.transform.position = hit.point;
            particleSystem.transform.forward = hit.normal;
            particleSystem.Play();
            points[lastRipple] = hit.point;
            times[lastRipple] = Time.time;
            lastRipple++;
            lastRipple %= MAX_POINTS;
            material.SetFloatArray("_times", times);
            material.SetVectorArray("_points", points);
            lastTime = Time.time;
        }
        material.SetFloat("_time", Time.time);
        
    }
}
